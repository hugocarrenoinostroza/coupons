package cl.bancoripley.coupons;

import org.apache.beam.sdk.io.jdbc.JdbcIO;
import org.apache.beam.sdk.io.jdbc.JdbcIO.DataSourceConfiguration;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.gson.Gson;

public class DataSourceConfigProvider {
	private String bucketName;

	public DataSourceConfigProvider(String bucketName) {
		this.bucketName = bucketName;
	}

	public DataSourceConfiguration getDataSourceConfig() {
		Storage storage = StorageOptions.getDefaultInstance().getService();

		Blob properties = storage.get(bucketName, "config/properties.json");

		String content = new String(properties.getContent());

		Gson g = new Gson();

		DataFlowProperties dataFlowProperties = g.fromJson(content, DataFlowProperties.class);

		return JdbcIO.DataSourceConfiguration.create(dataFlowProperties.getDriverClassName(),
				dataFlowProperties.getDatabaseUrl());
	}
}
