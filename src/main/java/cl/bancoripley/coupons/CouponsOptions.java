package cl.bancoripley.coupons;

import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.Validation.Required;
import org.apache.beam.sdk.options.ValueProvider;

public interface CouponsOptions extends PipelineOptions {
	@Description("Path of the file to read from")
	ValueProvider<String> getInputFile();

	void setInputFile(ValueProvider<String> value);

	@Description("Campaign ID")
	ValueProvider<Integer> getCampaignId();

	void setCampaignId(ValueProvider<Integer> value);
	
	@Description("Bucket for dataflow config")
	@Required
	String getBucketName();

	void setBucketName(String value);
}