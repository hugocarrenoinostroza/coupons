package cl.bancoripley.coupons;

import java.io.Serializable;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.jdbc.JdbcIO;
import org.apache.beam.sdk.io.jdbc.JdbcIO.DataSourceConfiguration;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;

class CouponCampaign implements Serializable {
	private static final long serialVersionUID = -7248883665202357418L;
	
	private Integer campaignId;
	private String coupon;

	public CouponCampaign(Integer campaignId, String coupon) {
		this.campaignId = campaignId;
		this.coupon = coupon;
	}

	public Integer getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}
}

class CreateCouponCampaign extends DoFn<String, CouponCampaign> {
	ValueProvider<Integer> campaignId;

	CreateCouponCampaign(ValueProvider<Integer> campaignId) {
		this.campaignId = campaignId;
	}

	@ProcessElement
	public void processElement(ProcessContext c) {
		c.output(new CouponCampaign(this.campaignId.get(), c.element()));
	}
}

public class LoadCoupons {

	public static void main(String[] args) {
		CouponsOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CouponsOptions.class);
		
		DataSourceConfigProvider dataSourceConfigProvider = new DataSourceConfigProvider(options.getBucketName());
		
		DataSourceConfiguration dataSourceConfiguration = dataSourceConfigProvider.getDataSourceConfig();
		
		Pipeline p = Pipeline.create(options);

		p.apply("Read CSV", TextIO.read().from(options.getInputFile()))
				.apply("Create CouponCampaign", ParDo.of(new CreateCouponCampaign(options.getCampaignId())))
				.apply("Load Coupons in Cloud SQL", JdbcIO.<CouponCampaign>write()
						.withDataSourceConfiguration(dataSourceConfiguration)
						.withStatement(
								"INSERT INTO coupons (couponCampaignId, code, active, createdAt, updatedAt) VALUES(?, ?, 1, ?, ?)")
						.withPreparedStatementSetter(new JdbcIO.PreparedStatementSetter<CouponCampaign>() {
							public void setParameters(CouponCampaign element, PreparedStatement query)
									throws SQLException {
								query.setInt(1, element.getCampaignId());
								query.setString(2, element.getCoupon());
								query.setDate(3, new Date(Calendar.getInstance().getTimeInMillis()));
								query.setDate(4, new Date(Calendar.getInstance().getTimeInMillis()));
							}
						}));

		p.run();
	}

}
